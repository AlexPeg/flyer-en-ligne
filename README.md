# Flyer en ligne 

# Gitlab

* https://gitlab.com/AlexPeg/flyer-en-ligne

# Démarrer projet

* git clone git@gitlab.com:AlexPeg/flyer-en-ligne.git


# Version en ligne

* http://same-grain.surge.sh/

# Maquette

* Maquette bureau

https://www.figma.com/file/Os3mw25mWrGX2daWSudyeU/Untitled?node-id=0%3A3&frame-preset-name=Desktop

* Maquette mobile
https://www.figma.com/file/f0VR8XwylATP2YM0Zyn8r6/Untitled?node-id=0%3A1&frame-preset-name=Android

# Eco-index

* http://www.ecoindex.fr/resultats/?id=120523

* eco-index: A

# Pagespeed

* https://developers.google.com/speed/pagespeed/insights/?hl=fr&url=http%3A%2F%2Fsame-grain.surge.sh%2F

* score: 91